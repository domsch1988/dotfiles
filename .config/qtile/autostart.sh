#!/bin/sh

run() {
  if ! pgrep -f "$1" ;
  then
    "$@"&
  fi
}

run "kanshi"
run "swaybg" -m fill -i /home/dosa/dotfiles/Bilder/Wallpaper_Dark_1.jpg
run "swayidle" -w
